import { createSSRApp } from 'vue'
import { createMemoryHistory } from 'vue-router';
import { createConfiguredRouter } from './router';
import { createConfiguredStore } from './store';
import App from './App';

export default (context) => {
    // since there could potentially be asynchronous route hooks or components,
    // we will be returning a Promise so that the server can wait until
    // everything is ready before rendering.
    return new Promise((resolve, reject) => {
        // create router and store instances
        const router = createConfiguredRouter(
            createMemoryHistory()
        );
        const store = createConfiguredStore();
        // sync so that route state is available as part of the store
        // sync(store, router);
        const app = createSSRApp(App)
            .use(store)
            .use(router);

        if (context.componentName) {
            store.dispatch('setPageByComponentName', {
                'name': context.componentName,
                'props': context.props || {},
            });
        }

        // set server-side router's location
        router.push(context.url);

        // wait until router has resolved possible async components and hooks
        router.isReady().then(() => {
            // This `rendered` hook is called when the app has finished rendering
            // After the app is rendered, our store is now
            // filled with the state from our components.
            // When we attach the state to the context, and the `template` option
            // is used for the renderer, the state will automatically be
            // serialized and injected into the HTML as `window.__INITIAL_STATE__`.
            context.rendered = () => context.state = store.state;
            // the Promise should resolve to the app instance so it can be rendered
            return resolve(app);
        }, reject);
    });
};
