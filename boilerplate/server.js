const express = require('express');
const {renderToString} = require("@vue/server-renderer");

const app = express();
const port = process.env.PORT || 3000;
const template = require('fs').readFileSync('./dist/client/index.html', 'utf-8');
const manifest = require("./dist/server/ssr-manifest.json");
const VueApp = require('./dist/server' + manifest['app.js']).default;

function injectMeta(html, meta) {
    if (meta.title) {
        const ssrTitle = `<title>${meta.title}</title>`;
        if (html.includes('<title>')) {
            html = html.replace(/<title>.*?<\/title>/i, ssrTitle, html);
        } else {
            html = html.replace('</head>', `${ssrTitle}</head>`, html);
        }
    }
    if (meta.description) {
        html = html.replace('</head>', `<meta name="description" content="${meta.description}"></head>`, html);
    }
    if (meta.keywords) {
        html = html.replace('</head>', `<meta name="keywords" content="${meta.keywords}"></head>`, html);
    }
    if (meta.imageUrl) {
        const metaImageTags = `<meta property="og:image" content="${meta.imageUrl}">`
            + `<meta property="twitter:image" content="${meta.imageUrl}">`
            + `<link rel="image_src" href="${meta.imageUrl}">`;
        html = html.replace('</head>', `${metaImageTags}</head>`, html);
    }

    return html;
}

app.use(express.static('dist/client', {index: false}));
app.get('/healthcheck', (req, res) => res.end('OK'));
app.get('*', (req, res) => {
    const context = {url: req.url};

    VueApp(context).then((app) => {
        renderToString(app).then(html => {
            let appContextState = context.rendered();
            const pageMeta = appContextState.PageStore.pageMeta || {};
            delete appContextState.PageStore.pageMeta;

            const script = `<script>window.__INITIAL_STATE__=${JSON.stringify(appContextState)}</script>`

            const result = template
                .replace(/<div id="app"([^>]+?)>/, '<div id="app"$1>' + html)
                .replace(/<body([^>]+?)>/, '<body$1>' + script);

            res.end(
                injectMeta(result, pageMeta)
            );
        }).catch(err => {
            if (err.code === 404) {
                res.status(404).end('Page not found');
            } else {
                res.status(500).end('Internal Server Error');
                console.error(err);
            }
        });
    }, () => {
        res.status(500).end('Internal Server Error!');
    });

});
app.listen(port, () => console.log(`http://localhost:${port}`));
