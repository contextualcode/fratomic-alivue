// this is just an example of how you can use promise for dynamic configuration of component
module.exports = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            "title": "Button",
            "label": "Buttons",
            "context": {
                "buttonText": "Button Text random " + Math.random()
            },
            "status": "exported"
        });
    }, 100);
});
