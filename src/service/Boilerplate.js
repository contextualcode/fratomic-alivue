'use strict';

const path = require('path');
const fs = require('fs');
const readline = require('readline');
const FilesystemHelper = require('../helper/FilesystemHelper');

class Boilerplate {
	boilerplate(outputRootPath) {
		const rl = readline.createInterface(process.stdin, process.stdout);
		rl.question(`This can override your files in "${outputRootPath}", continue? Y/[N]: `, answer => {
			rl.close();
			if (answer && answer.toLowerCase() === 'y') {
				this.copyBoilerplateFiles(outputRootPath);
				this.boilerplateAskClearDocker(outputRootPath);
			} else {
				console.log('Exit');
			}
		});
	}

	boilerplateAskClearDocker(outputRootPath) {
		const rl = readline.createInterface(process.stdin, process.stdout);
		rl.question(`Keep docker files? Y/[N]: `, answer => {
			if (answer && answer.toLowerCase() === 'n') {
				this.removeDockerFiles(outputRootPath);
			}
			console.log('Done');
			rl.close();
		});
	}

	copyBoilerplateFiles(outputRootPath) {
		FilesystemHelper.copyFolderRecursiveSync(
			path.resolve(__dirname, '..', '..', 'boilerplate'),
			outputRootPath
		);

		fs.renameSync(
			path.resolve(outputRootPath, 'boilerplate.gitignore'),
			path.resolve(outputRootPath, '.gitignore')
		);
	}

	removeDockerFiles(outputRootPath) {
		FilesystemHelper.deleteFolderRecursive(path.join(outputRootPath, 'docker'), true);
		fs.unlinkSync(path.join(outputRootPath, '.dockerignore'));
		console.log('Remove .dockerignore');
	}
}

module.exports = Boilerplate;
